<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function User()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	
	 public function Price()
	{
		return $this->belongsTo('App\Price', 'price_id');
	}
	
}
