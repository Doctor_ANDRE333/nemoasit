<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
     public function Order()
	{
		return $this->hasMany('App\Order', 'price_id');
	}
}
