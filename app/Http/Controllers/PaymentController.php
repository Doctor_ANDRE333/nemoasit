<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PayerInfo;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use Session;
use Redirect;
use URL;

use App\User;
use App\Donate;

class PaymentController extends Controller
{    
	private $_api_context;
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
		
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    
    public function payWithpaypal(Request $request)
    {
		$this->validate($request, [
			'amount' => 'required|numeric',
		]);
		
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
 
        $item_1 = new Item();
 
        $item_1->setName('Item 1') /** item name **/
            ->setCurrency('RUB')
            ->setQuantity(1)
            ->setPrice($request->get('amount')); /** unit price **/
 
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
		
		//TESTTTTTTTTT
		\Session::put('balance', $request->get('amount'));
 
        $amount = new Amount();
        $amount->setCurrency('RUB')
            ->setTotal($request->get('amount'));
 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
 
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('status'));
 
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } 
		catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('paywithpaypal');
            } else {
 
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
            }
        }
 
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
 
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
 
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
 
        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');
    }
    
    public function getPaymentStatus(Request $request)
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
		//dd(Session::all());
		
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty($request->PayerID) || empty($request->token)) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('/');
        }
 
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);
 
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
 
        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
			
			$id = Auth::user()->id;
			$user = User::find($id);
			$user->balance = $user->balance + Session::get('balance');
			$user->save();
			
			$donate = new Donate();
			$donate->method = 'PayPal';
			$donate->number = Session::get('balance');
			$donate->user_id = $user->id;
			$donate->save();
			
            return Redirect::route('selection');
        }
 
        \Session::put('error', 'Payment failed');
        return Redirect::route('selection');
    }
}