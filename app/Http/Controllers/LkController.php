<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Comment;
use App\Answer;

class LkController extends Controller
{
    public function updateMe(Request $request, $id)
    {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
            'surname' => 'required|max:25',
            'phone' => 'numeric|max:89999999999'
		]);

        if (count(User::where('email', '=', $request->email)->where('id', '<>', $id)->get()) != 0){ 
            session()->flash('message', 'Данный email уже используется!');
            return redirect()->route('aboutMe', $id);
        }
        else
		{
            $i = User::find($id);
            
            $i->name = $request->name;
            $i->surname = $request->surname;
            $i->email = $request->email;
            $i->phone = $request->phone;
			
			if($request->file('avatar'))
			{     
				$sizeMB = filesize($request->file('avatar'));
				$sizeIMG = getimagesize($request->file('avatar'));
				
				if ($sizeMB > 3145728)
				{
					session()->flash('error', 'Размер файла не должен превышать 3 мегабайт!');
					return redirect('/lk/aboutMe');
					
				}
				else if ($sizeIMG[0] != $sizeIMG[1])
				{
					session()->flash('error', 'Изображение должно быть квадратным (1:1)!');
					return redirect('/lk/aboutMe');
					
				}
				else
				{
					$file = $request->file('avatar');
					$filename = Auth::user()->surname . "_" . Auth::user()->name . "_token:" . Auth::user()->id . ".jpg";
					$location = public_path() . '/Avatars';
					$file->move($location,$filename);
					$i->avatar = "Avatars/" . $filename; 
				}
				
			}
			else
			{
            session()->flash('error', 'Возникли проблемы с загрузкой файла :/');
			}


            if ($request->passCheck == 1){
                $this->validate($request, [
					'password' => 'required|min:6|confirmed'
				]);
                
                $i->password = bcrypt($request->password);
            }
			
            $i->save();
            session()->flash('message', 'Личные данные успешно обновлены!');
            return redirect('/lk/aboutMe');
        }
    }
	
	public function makeComment(Request $request)
    {
        $this->validate($request, [
			'text' => 'required|max:255',
		]);
		
		$comment = new Comment;
		
        $comment->text = $request->text;
		$comment->user_id = $request->user_id;
		
		$comment->save();
		session()->flash('message', 'Новый комментарий успешно добавлен!');
		
		return redirect()->route('indexChats');
    }
	
	 public function sendAnswer(Request $request, $comment_id){
        $this->validate($request, [
			'text' => 'required|max:255',
		]);
		$answer = new Answer;
		
        $answer->text = $request->text;
		$answer->user_id = $request->user_id;
        $answer->comment_id = $comment_id;
		
		$answer->save();
		session()->flash('message', 'Новый ответ успешно добавлен!');
		
		return redirect()->route('indexChats');
    }


		
	public function aboutMeLK(){
		$user = Auth::user();
		return view('admin/aboutMe', ['user' => $user]);
	}
	
	 public function indexChats()
    {
        if (Auth::user()->role == 'default' || Auth::user()->role == 'redactor'){
            $comments = Comment::where('user_id', '=', Auth::user()->id)->get();
            $answers = collect();
            foreach($comments as $comment){
                foreach($comment->answers as $answer){
                    $answers->push($answer);
                }
            }
			
            //dd(Auth::user()->Comments[0]);

            if (count($comments) > 0){
                $ThisComment = Auth::user()->Comments[0];

                $comments = $comments->merge($answers);
                $comments = $comments->sortBy('created_at');

                return view('admin.chat', ['comments' => $comments, 'ThisComment' => $ThisComment]);
            }
            else{
                $comments = $comments->merge($answers);
                $comments = $comments->sortBy('created_at');

                return view('admin.chat', ['comments' => $comments]);
            }
        }
        else if (Auth::user()->role == 'comitet'){
            $comments = collect();
            $users = User::where('role', '<>', 'comitet')->get();

            foreach($users as $user){
                foreach($user->Comments as $comment){
                    $comments->push($comment);
                }
            }

            return view('admin.chats', ['comments' => $comments]);
        }
    }
	

		public function indexCurrentChat($id){
        if (Auth::user()->role == 'comitet'){
           $comment = Comment::find($id);
           $ThisComment = $comment;
           $comments = collect();
           $comments = $comment->Answers;
           $comments->push($comment);
           $comments = $comments->sortBy('created_at');
           return view('admin.chat', ['comments' => $comments, 'ThisComment' => $ThisComment]);
        }
		else 
		{
			 session()->flash('warning', 'Вы не являетесь членом Орг.Комитета!');
			return redirect('/');
		}
    }


	

	
	

}
