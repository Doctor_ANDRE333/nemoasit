<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Order;
use App\Donate;

use YooKassa\Client;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use YooKassa\Model\NotificationEventType;

use Session;
use Redirect;
use URL;

class YooKassaController extends Controller{
    public function payCreate(Request $req){
		$this->validate($req, [
			'amount' => 'required|numeric',
		]);
		
		$clientId = '823632';
		$clientSecret = 'test_k033mOebX5V6-hzP-Tl31_vqH7KUlUE7uD9OYGDXnyE';
		
		\Session::put('balance', $req->get('amount'));

		$client = new Client();
		$client->setAuth($clientId, $clientSecret);
		
		$payment = $client->createPayment([
			'amount' => [
				'value' => $req->amount,
				'currency' => 'RUB',
			],
			'description' => 'Приобретение валюты', // Прописываем нужное описание
			'capture' => true,
			'confirmation' => [
				'type' => 'redirect',
				'return_url' => 'https://nemoasit.ru/pay/callback', // Задаём страницу на которую пользователь вернётся если нажмёт книпку вернутся в магазин на сайте yooMoney
			],
			'metadata' => [
				'order_id' => 1, // Передаём номер заказа например через $rec->amount
			],
		], uniqid('', true));
		
		$user = Auth::user();
		$user->balance = $user->balance + Session::get('balance');
		$user->save();
		
		$donate = new Donate();
		$donate->method = 'PayPal';
		$donate->number = Session::get('balance');
		$donate->user_id = $user->id;
		$donate->save();
		
    	return redirect( $payment->getConfirmation()->getConfirmationUrl() );
    }

    public function payCallback(){
		return Redirect::route('selection');	
    }
}
