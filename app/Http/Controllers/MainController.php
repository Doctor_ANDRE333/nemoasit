<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use WebMoneyMerchant;
use App\Price;
use App\User;
use App\Order;
use Session;
use Redirect;
use URL;
use App\Review;

class MainController extends Controller
{
	//private $payment_no = 0;
	
    public function indexPrices(){
		
		// ключ положить в ENV!!!!!!!!!
		
		$fields = ['key' => 'd8f080ec3e2dd73531a4ff8fa23aa28a', 'action' => 'services'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://wiq.ru/api/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$results = curl_exec($ch);

		$parsed = json_decode($results);
	/*	foreach ($parsed as $solo)
		{
			$pr = new Price;
			$pr->id = $solo->ID;
			$pr->name = $solo->name;
			$pr->description = $solo->description;
			$pr->cost = $solo->cost;
			$pr->min = $solo->min;
			$pr->max = $solo->max;
			$pr->save();
			
		}
		*/
		
		return view('app/prices', ['goods' => $parsed]);
	}
	
	//public function makePaymentWithPayPal(){
	//	return view('app/paywithpaypal');
	//}
	
	public function selectPaymentMethod(){
		return view('app/selectPaymentMethod');
	}

	
	/*public function makePayPalPayment(){
		return view('app/paywithpaypal');
	}
	
	public function makeWebMoneyPayment(){
		return view('app/paywithwebmoney');
	}
	
	public function makeYooKassaPayment(){
		return view('app/paywithyookassa');
	}*/
	
	public function goToWebmoneyMethods(Request $request){
		echo WebMoneyMerchant::generatePaymentForm($request->amount, 23, 'test');
		\Session::put('balance', $request->get('amount'));
		\Session::put('user_id', Auth::user()->id);
	}
	
	public function check(Request $request){
		//return WebMoneyMerchant::payOrderFromGate($request);
	}
	
	public function indexOrder(Request $request){
		//dd($request->all());
		$prices = Price::all();
		
		if ($request->category){
			$prices = $prices->where('category', '=', $request->category);
		}
		
		//$prices = Price::all();
		return view('app/order', ['prices' => $prices]);
	}
	
	public function indexStatusOrder(){
		$orders = Order::where('user_id', '=', Auth::user()->id)->get();
	
		$key = 'b7b4c086e40cca9e894e627ace63a8f5';
		$stArr = [];
		
		foreach ($orders as $order)
		{
			$fields = ['key' => $key, 
					'action' => 'status',
					'order' => $order->order_id,
					];
					
			$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://wiq.ru/api/');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					
			$results = curl_exec($ch);
			
			$parsed = json_decode($results);
			$price_name = Price::find($parsed->service)->name;
			$stage = (object) array('id' => $order->order_id, 
									'quantity' => $parsed->quantity, 
									'link' => $parsed->link, 
									'service' => $price_name,
									'remains' => $parsed->remains,
									'status' => $parsed->status,
									'start_count' => $parsed->start_count,
									'charge' => $parsed->charge);
			
			array_push($stArr, $stage);
		}
		
		$stArr = array_reverse($stArr);
		
		return view('app/statusOrder', ['orders' => $stArr]);
	}
	
	
	
	public function cancelOrder(int $order_id)
	{
		$key = 'b7b4c086e40cca9e894e627ace63a8f5';
		$fields = ['key' => $key, 
					'action' => 'cancel',
					'order' => $order_id,
					];
					
			$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://wiq.ru/api/');
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					
			$results = curl_exec($ch);
			
			$parsed = json_decode($results);
			
		//	dd($parsed);
		session()->flash('message', 'Заказ ' . $order_id . ' отменён');
		return redirect('/statusOrder');
		
	}
	
	public function makeOrder(Request $request){
		
		$this->validate($request, [
			'amount' => 'required|numeric',
			'url' =>'required|active_url',
			'order_type' => 'required'	
		]);
			
		//dd($request->all());		
		$price_id = $request->order_type; //id услуги
		$min = Price::find($price_id)->min;
		$max = Price::find($price_id)->max;
		$amount = ($request->amount)/1000; //кол-во
		
		$pricePerThousand = Price::find($price_id)->cost; //Цена за 1к
		$minus = $pricePerThousand * $amount;
		
		if (Auth::user()->balance < $minus)
		{
			session()->flash('error', 'Ошибка! У вас недостаточно средств, пополните баланс :(');
			return redirect('/order');
		}
		else if ($request->amount < $min)
		{
			session()->flash('error', 'Ошибка! Минимальное количество ' . $min);
			return redirect('/order');
		}
		else if ($request->amount > $max)
		{
			session()->flash('error', 'Ошибка! Максимальное количество ' . $max);
			return redirect('/order');
		}
		else
		{
			$id = Auth::user()->id;
			$user = User::find($id);
			
			$user->balance = $user->balance - $minus;
			$user->save();
			
			$key = 'b7b4c086e40cca9e894e627ace63a8f5';
			$url = 'https://wiq.ru/api/key=' . $key . 
			'&action=create&service=' . $request->order_type .
			'&quantity=' . $request->amount .
			'&link=' . $request->url;
			

			$fields = ['key' => $key, 
				'action' => 'create',
				'service' => $request->order_type,
				'quantity' => $request->amount,
				'link' => $request->url
				];
			
			$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://wiq.ru/api/');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				
			$results = curl_exec($ch);
			$parsed = json_decode($results); //order
			

				$order = new Order;
				$order->order_id = $parsed->order;
				$order->user_id = Auth::user()->id;
				$order->price_id = $price_id;
				$order->amount = $request->amount;
				$order->save();
				
			return redirect('/');
		}
	}
	
	public function makeReview(Request $request)
	{
			$review = new Review;
			$review->text = $request->review_text;
			$review->user_id = Auth::user()->id;
			$review->save();
			
			session()->flash('message', 'Создан новый отзыв.');
			return redirect('/');
		
	}
	

}
