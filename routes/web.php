<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\YooKassaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app/main');
})->name('main');

Route::get('/prices', 'MainController@indexPrices')->name('prices');
Route::get('/lk/aboutMe', 'LkController@aboutMeLK')->name('aboutMe');
Route::get('/lk/chats', 'LkController@indexChats')->name('indexChats');
Route::get('lk/chats/{id}', 'LkController@indexCurrentChat')->name('indexCurrentChat');

//Route::get('/payment', 'MainController@makePaymentWithPayPal')->name('paymentWithPayPal');

Route::get('/paymentMethod', 'MainController@selectPaymentMethod')->name('selection');
//Route::get('/paypalMethod', 'MainController@makePayPalPayment')->name('paypalMethod');        //В силу модального окна
//Route::get('/webmoneyMethod', 'MainController@makeWebMoneyPayment')->name('webmoneyMethod');
//Route::get('/yookassaMethod', 'MainController@makeYooKassaPayment')->name('yookassaMethod');


Route::get('/webmoneyPayments', 'MainController@goToWebmoneyMethods')->name('webmoneyPayments');
Route::get('/order', 'MainController@indexOrder')->name('order');
Route::get('/statusOrder', 'MainController@indexStatusOrder')->name('statusOrder');
Route::get('/cancelOrder/{order_id}', 'MainController@cancelOrder')->name('cancelOrder'); 

Route::post('/updateMe/{id}', 'LkController@updateMe')->name('updateMe');
Route::post('/makeReview', 'MainController@makeReview')->name('makeReview');
Route::post('paypal', 'PaymentController@payWithpaypal')->name('paypal');
Route::get('status', 'PaymentController@getPaymentStatus')->name('status');

Route::post('/lk/answer/{id}', 'LkController@sendAnswer')->name('SendAnswer');
Route::post('/makeComment', 'LkController@makeComment')->name('makeComment');
Route::post('/makeorder', 'MainController@makeOrder')->name('makeOrder');

Route::post('/webmoney/result', 'WebMoneyExampleController@payOrderFromGate');
//Route::get('/webmoney/result',  'WebMoneyExampleController@payOrderFromGateOK');

Route::post('/checker', 'MainController@check')->name('check');


//YooKassa

Route::any('/pay', 'YooKassaController@payCreate')->name('pay.create');
Route::any('/pay/callback', 'YooKassaController@payCallback')->name('pay.callback');

/*Route::get('handle-payment', 'PayPalPaymentController@handlePayment')->name('make.payment');

Route::get('cancel-payment', 'PayPalPaymentController@paymentCancel')->name('cancel.payment');

Route::get('payment-success', 'PayPalPaymentController@paymentSuccess')->name('success.payment');

Route::get('/payment/status',['as' => 'status', 'uses' => 'PaymentController@getPaymentStatus']);*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
