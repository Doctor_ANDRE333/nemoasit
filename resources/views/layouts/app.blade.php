<!DOCTYPE html>
<html>
	<head>
		<title> @yield('name') </title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="public/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
		<link rel="stylesheet" href="{{ asset('styles/mainStyle.css') }}">
		<link rel="stylesheet" href="{{ asset('styles/auth.css') }}">
		
		<link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@300;400&display=swap" rel="stylesheet">
		<meta name="csrf-token" content="{{ csrf_token() }}">
	</head>

	<body>	
		<nav class="navbar navbar-dark navbar-expand-md navigation-clean-search fixed-top">
			<div class="container">
				<img src = "public/images/nemo_fish_binary3.png" width = "8%">
				<a class="navbar-brand ml-3" style = "color: white;" href="/">NemoAS<span style = "color: #BF7130;">IT</span> </a>
				<button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navcol-1">
					<ul class="nav navbar-nav">
						<li class="nav-item left-part" role="presentation"><a class="nav-link active" href="{{ route('prices') }}">Прайс-лист</a></li>
						@if (Auth::user())
							<li class="nav-item left-part" role="presentation"><a class="nav-link active" href="{{ route('order') }}">Оформить заказ</a></li>
							<li class="nav-item left-part" role="presentation"><a class="nav-link active" href="{{ route('statusOrder') }}">Статус заказа</a></li>
						@endif
						<li class="nav-item left-part" role="presentation">
							<!--<div class="dropdown">
								<a class="nav-link active dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Ещё
								</a>
								<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
									<button class="dropdown-item" type="button">Действие</button>
									<div class="dropdown-divider"></div>
									<button class="dropdown-item" type="button">Другое действие</button>
									<div class="dropdown-divider"></div>
									<button class="dropdown-item" type="button">Что-то еще здесь</button>
								</div>
							</div>-->
						</li>
					</ul>
					<ul class="navbar-nav ml-auto">
						<!-- Authentication Links -->
						@guest
							<li class="nav-item">
								<a class="nav-link text-light" href="{{ route('login') }}">{{ __('Login') }}</a>
							</li>
							@if (Route::has('register'))
								<li class="nav-item">
									<a class="nav-link text-light" href="{{ route('register') }}">{{ __('Register') }}</a>
								</li>
							@endif
						@else
							<li class = "nav-item">
								<a class="nav-link text-light" href="{{ route('selection') }}">
									{{ __('Пополнить баланс') }} <i class="fas fa-coins coins"></i>
								</a>
							</li`>
						
							<li class="nav-item dropdown">
								<a id="navbarDropdown" style = "display: inline-block" class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									{{ Auth::user()->name }}
								</a>
								<span class="badge balance" style = "display: inline-block"> {{ round(Auth::user()->balance, 2) }} ₽ </span>

								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="{{ route('aboutMe') }}">
										{{ __('Личный кабинет') }}
									</a>
								
									<a class="dropdown-item" href="{{ route('logout') }}"
										onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
										{{ __('Выйти') }}
									</a>
									
									<!--<a class="dropdown-item" href="{{ route('selection') }}">
										{{ __('Пополнить баланс') }}
									</a>-->

									<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
										{{ csrf_field() }}
									</form>
								</div>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>
		
		@yield('content')
		
		<footer class="footer">
			<div class="container">
				<div class = "row">
					<div class = "col-12 labelCont border-bottom border-secondary mb-3">
						<h4 class = "text-light"> Контакты </h4>
					</div>

					<div class = "col-12 contactInfo">
						<div>
							<i class="fas fa-phone-square-alt footer-info"></i> <p>  Тел: +7 (916) 914-37-29  </p>
						</div>

						<div>
							<i class="fas fa-map-marker-alt footer-info"></i> <p>  Адрес: г. Москва, ул. 15-Парковая  </p>
						</div> 

						<div>
							<i class="fas fa-mail-bulk footer-info"></i> <p> ChinAkhverdiev@mail.ru  </p>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<script src="public/jquery/jquery-3.2.1.min.js"></script>
		<script src="public/js/bootstrap.bundle.min.js"></script>
		
		<script>
			function onEntry(entry) {
			  entry.forEach(change => {
				if (change.isIntersecting) {
				  change.target.classList.add('element-show');
				}
			  });
			}
			let options = { threshold: [0.5] };
			let observer = new IntersectionObserver(onEntry, options);
			let elements = document.querySelectorAll('.element-animation');
			for (let elm of elements) {
			  observer.observe(elm);
			}	
		</script>
	</body>
	
	<style>
		.balance{
			color: white;
			background-color: #BF7130;
		}
		
		.coins{
			color: #BF7130;
		}
	</style>
</html>