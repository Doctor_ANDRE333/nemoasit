@extends('layouts.app')

@section('name')
	Прайс-лист
@endsection

@section('content')
	<div class = "container maininfo">
		<h1 class = "text-center label"> ЦЕНЫ </h1>
		<div class = "table-responsive pt-3 pb-2">
			<table class="table table-striped">
				<thead class="thead-light price-header">
					<tr class = "text-center">
						<th scope="col">ID</th>
						<th scope="col" style = "vertical-align: middle;">Услуга</th>
						<th scope="col" style = "vertical-align: middle;">Описание</th>
						<th scope="col" style = "vertical-align: middle;">Цена</th>
						<th scope="col" style = "vertical-align: middle;">Мин / Макс</th>
					</tr>
				</thead>

				<tbody>	
					@foreach ($goods as $good)
						<tr class = "text-center">
							<td> <b> {{ $good->ID }} </b> </td>
							<td> <b> {{ $good->name }} </b> </td>
							<td class = "text-justify"> <b> {{ $good->description }} </b ></td>
							<td> <h4 class = "price"> {{ $good->cost }} </h4> </td>
							<td> <b> {{ $good->min }} / {{ $good->max }} </b> </td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection