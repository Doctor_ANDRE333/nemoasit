@extends('layouts.app')

@section('name')
	Статус заказов
@endsection

@section('content')
	<div class = "container-fluid" style = "margin-top: 150px; min-height: 710px;">
		<div class = "row">
			<div class = "col-6" style = "padding-top: 12px">
				<h1 class = "text-center label float-left"> ЗАКАЗЫ </h1> 
			</div>
			<div class = "col-6">
				<a class = "btn float-right" style = "font-size: 38px; color: #BF7130"> <i class="fas fa-sync ml-2" onclick='window.location.reload()'></i> </a>
			</div>
		</div>
		
		@include('common.errors')
		
		@if(Session::has('message'))
			<div class="alert alert-arrow d-flex rounded p-0 fixed-top" style = "margin-top: 82px" role="alert">
				<div class="alert-icon d-flex justify-content-center align-items-center text-white flex-grow-0 flex-shrink-0">
					<i class="fa fa-check"></i>
				</div>
				<div class="alert-message d-flex align-items-center py-2 pl-4 pr-3">
					{{ Session::get('message') }}
				</div>
				<a href="#" class="close d-flex ml-auto justify-content-center align-items-center px-3" data-dismiss="alert">
					<i class="fas fa-times"></i>
				</a>
			</div>
		@endif
		
		@if (count($orders) > 0)
			<div class = "table-responsive pt-3 pb-2">
				<table class="table table-striped">
					<thead class="thead-light price-header">
						<tr class = "text-center">
							<th scope="col"style = "vertical-align: middle;">ID</th>
							<th scope="col" style = "vertical-align: middle;">Вид накрутки</th>
							<th scope="col" style = "vertical-align: middle;">Цена</th>
							<th scope="col" style = "vertical-align: middle;">Количество</th>
							<th scope="col" style = "vertical-align: middle;">Ссылка</th>
							<th scope="col" style = "vertical-align: middle;">Остаток</th>
							<th scope="col" style = "vertical-align: middle;">Было</th>
							<th scope="col" style = "vertical-align: middle;">Статус</th>
							<th scope="col" style = "vertical-align: middle;">Действие</th>
						</tr>
					</thead>

					<tbody>	
						@foreach ($orders as $order)
						<tr class="text-center">
							<td>{{ $order->id }}</td>
							<td>{{ $order->service }}</td>
							<td>{{ $order->charge }}р</td>
							<td>{{ $order->quantity }}</td>
							<td>{{ $order->link }}	</td>
							<td>{{ $order->remains }}  </td>
							<td>{{ $order->start_count }}  </td>
							<td>{{ $order->status }}  </td>
							<td><a href="{{ route('cancelOrder', $order->id) }}" class="form-control mt-2">Отменить</a></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<div class="alert alert-danger mt-3"> На текущий момент у Вас нет заказов! </div>
			@endif
		</div>
	</div>
@endsection