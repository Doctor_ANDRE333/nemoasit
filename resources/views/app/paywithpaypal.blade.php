@extends('layouts.app')

@section('name')
	Пополнение счета PayPal
@endsection

@section('content')		
	<div class="container" style = "margin-top: 150px; min-height: 710px;">
		<div class="row">
			<aside class="col-sm-6" style = "margin: 0 auto">
				<article class="card">
					<div class="card-body p-5">
						@include('common.errors')
						@if ($message = Session::get('success'))
							<div class="alert alert-primary" role = "alert">
								<span onclick="this.parentElement.style.display='none'" class="w3-button w3-green w3-large w3-display-topright">&times;</span>
								<p>{!! $message !!}</p>
							</div>
							<?php Session::forget('success');?>
						@endif

						@if ($message = Session::get('error'))
							<div class="alert alert-danger" role = "alert">
								<span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">&times;</span>
								<p>{!! $message !!}</p>
							</div>
							<?php Session::forget('error');?>
						@endif

						<form role="form" method = "POST" action = "{{route('paypal')}}">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="amount">Money, ₽</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-credit-card"></i></span>
									</div>
									<input type="text" class="form-control" name="amount">
								</div> <!-- input-group.// -->
							</div> <!-- form-group.// -->

							<div class="parent-wrapper">
								<div class="parent">
									<button class="btn child"> PayPal  </button>
								</div>
							</div>
						</form>
					</div> <!-- card-body.// -->
				</article> <!-- card.// -->
			</aside> <!-- col.// -->
		</div> <!-- row.// -->
	</div> 
	
	<style>
		.balance{
			color: white;
			background-color: #BF7130;
		}
		
		.parent-wrapper {
			height: 100%;
			width: 100%;
		}
		
		.parent {
			display: flex;
			font-size: 0;
			flex-wrap: wrap;
			margin: -10px 0 0 -10px;
		}
		
		.child {
			display: inline-block;
			margin: 10px 0 0 10px;
			flex-grow: 1;
		}
	</style>
@endsection