@extends('layouts.app')

@section('name')
	Создание заказа
@endsection

@section('content')		
	<div class="container maininfo" style = "min-height: 750px">
		<h1 class = "text-center label"> Оформление заказа </h1>
		<div class="row">
			<aside class="col-sm-6" style = "margin: 0 auto">
				<article class="card">
					<div class="card-body p-5">
						@include('common.errors')
						
						@if ($message = Session::get('error'))
							<div class="alert alert-arrow alert-arrow-danger d-flex rounded p-0 fixed-top" style = "margin-top: 82px" role="alert">
								<div class="alert-icon d-flex justify-content-center align-items-center text-white flex-grow-0 flex-shrink-0">
									<i class="fas fa-times-circle"></i>
								</div>
								<div class="alert-message d-flex align-items-center py-2 pl-4 pr-3">
									{!! $message !!}
								</div>
								<a href="#" class="close d-flex ml-auto justify-content-center align-items-center px-3" data-dismiss="alert">
									<i class="fas fa-times"></i>
								</a>
							</div>
							
							<?php Session::forget('error');?>
						@endif
						<form role="form" method = "GET">
							<div class="form-group">
							
							<h4 class = "label text-center"><label for="amount"><i class="fas fa-coins"></i><b>&#160;Ваш баланс: {{ round(Auth::user()->balance, 2) }}₽</b></label></h4>
							
							<label class = "mb-0" for="amount"><b>Выберите категорию</b></label>
							<div class="row">
								<div class="col-md-8 col-12">
									<select class="input-md form-control" id = "category" name="category">
										<!--<option disabled="" selected="" id="sel">Выберите категорию</option>-->
										<option <?if(isset($_GET['category']) and 19==$_GET['category']):?>selected="selected"<?endif?> value="19">Подписчики Instagram</option>
										<option <?if(isset($_GET['category']) and 20==$_GET['category']):?>selected="selected"<?endif?> value="20">Лайки Instagram</option>
										<option <?if(isset($_GET['category']) and 21==$_GET['category']):?>selected="selected"<?endif?> value="21">Авто лайки/просмотры</option>
										<option <?if(isset($_GET['category']) and 22==$_GET['category']):?>selected="selected"<?endif?> value="22">Просмотры Instagram</option>
										<option <?if(isset($_GET['category']) and 24==$_GET['category']):?>selected="selected"<?endif?> value="24">Комментарии Instagram</option>
										<option <?if(isset($_GET['category']) and 25==$_GET['category']):?>selected="selected"<?endif?> value="25">Статистика Instagram</option>
										<option <?if(isset($_GET['category']) and 31==$_GET['category']):?>selected="selected"<?endif?> value="31">TikTok</option>
										<option <?if(isset($_GET['category']) and 32==$_GET['category']):?>selected="selected"<?endif?> value="32">ВКонтакте</option>
										<option <?if(isset($_GET['category']) and 33==$_GET['category']):?>selected="selected"<?endif?> value="33">Telegram</option>
										<option <?if(isset($_GET['category']) and 34==$_GET['category']):?>selected="selected"<?endif?> value="34">YouTube</option>
										<option <?if(isset($_GET['category']) and 37==$_GET['category']):?>selected="selected"<?endif?> value="37">Likee</option> 
									</select>
								</div>
								
								<div class="col-md-4 col-12 preButton">
									<button class = "form-control confirm" type = "submit"> Выбрать </button>
								</div>
							</div>
						</form>
							
						<form role="form" method = "POST" action = "{{ route('makeOrder') }}">
							{{ csrf_field() }}
							<label  class = "mb-0 mt-2" for="amount"><b>Выберите услугу</b></label>
							<div class="row">
								<div class="col-12">
									<select name="order_type" id = "order_type" class="form-control">
										<option disabled="" selected="" id="sel">Выберите услугу</option>
										@foreach ($prices as $price)
											<option value="{{ $price->id }}"> {{$price->name}} - {{$price->cost}}₽</option>
										@endforeach
									</select>
								</div>
							</div>
								
							<label class = "mb-0 mt-2" for="amount"><b>Ссылка</b></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-at"></i></span>
								</div>
								<input type="text" class="form-control" name="url">
							</div>
							
							<label class = "mb-0 mt-2" for="amount"><b>Количество</b></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-sort-numeric-up-alt"></i></span>
								</div>
								<input type="number" id = "amount" value=0 class="form-control" name="amount">
							</div>
						
							<label class = "mb-0 mt-2" for="amount"><b>Цена заказа, ₽</b></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fa fa-credit-card"></i></span>
								</div>
								<input type="text" disabled="" id = "money" class="form-control" name="amount">
							</div>

							<div class="parent-wrapper mt-3">
								<div class="parent">
									<button class="btn child confirm"> Оформить заказ  </button>
								</div>
							</div>
						</form>
					</div> <!-- card-body.// -->
				</article> <!-- card.// -->
			</aside> <!-- col.// -->
		</div> <!-- row.// -->
	</div> 
	
	<script>
		function sum(){
			let moneyBox = document.getElementById('money');
			
			moneyBox.innerHTML = "";
			
			//console.log(order_type.option:selected);
			let selected = $('#order_type').find('option:selected');

			let selectedText = selected[0].innerText;
		
			let selectedTextWithoutP = selectedText.substr(0, selectedText.length - 1);
			//console.log(selectedTextWithoutP); //Строка вся, без рубля
			
			let strFromBegin = 0;
			
			for (let i = selectedTextWithoutP.length - 1; i >= 0; i--){
				if (selectedTextWithoutP[i] === 0 || selectedTextWithoutP[i] == 1 || selectedTextWithoutP[i] == 2 || selectedTextWithoutP[i] == 3 || selectedTextWithoutP[i] == 4 || selectedTextWithoutP[i] == 5 || selectedTextWithoutP[i] == 6 || selectedTextWithoutP[i] == 7 || selectedTextWithoutP[i] == 8 || selectedTextWithoutP[i] == 9){
					strFromBegin++;
				}
				else{
					break;
				}
			}

			let minus = -strFromBegin;
			
			let Summa = selectedTextWithoutP.substr(minus);
			//console.log(Summa); //Корректно
			
			let amountInput = document.getElementById('amount');
			
			let amountPerThousand = amountInput.value / 1000;
			
			let price = amountPerThousand * Summa;
			//console.log(price)
	
			if (price >= 0){
				moneyBox.value = price.toFixed(2) + " ₽";
			}else{
				moneyBox.value = "Цена не определена!";
			}
		}
	
		setInterval(sum, 1000);
	</script>
	
	<style>
		@media (max-width:900px){
			.preButton{
				margin-top: 10px;
			}
		}
	
		i{
			color: #BF7130;
		}
	
		.confirm{
			background-color: #BF7130;
			color: white;
		}
	
		.balance{
			color: white;
			background-color: #BF7130;
		}
		
		.parent-wrapper {
			height: 100%;
			width: 100%;
		}
		
		.parent {
			display: flex;
			font-size: 0;
			flex-wrap: wrap;
			margin: -10px 0 0 -10px;
		}
		
		.child {
			display: inline-block;
			margin: 10px 0 0 10px;
			flex-grow: 1;
		}
	</style>
@endsection