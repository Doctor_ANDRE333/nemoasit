@extends('layouts.app')

@section('name')
	Главная страница
@endsection

@section('content')


	<div class = "container-fluid video-part">
		<video poster="public/images/preview.png" playsinline autoplay muted loop>
			<source src="public/videos/fish.webm" type="video/webm">
			<source src="public/videos/fish.mp4" type="video/mp4">
		</video>	
	</div>
		@include('common.errors')
		
		@if(Session::has('message'))
			
			<div class="alert alert-arrow d-flex rounded p-0 fixed-top" style = "margin-top: 82px" role="alert">
				<div class="alert-icon d-flex justify-content-center align-items-center text-white flex-grow-0 flex-shrink-0">
					<i class="fa fa-check"></i>
				</div>
				<div class="alert-message d-flex align-items-center py-2 pl-4 pr-3">
					{{ Session::get('message') }}
				</div>
				<a href="#" class="close d-flex ml-auto justify-content-center align-items-center px-3" data-dismiss="alert">
					<i class="fas fa-times"></i>
				</a>
			</div>
		@endif
		
	
	<div class = "container main-part element-animation">
		<h2 class = "text-center mt-3 mb-3 label"> Наши преимущества </h2>
		
		<div class = "row">
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/reliability_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Надежность</h5>
					<hr>
					<p class="card-text text-justify">Мы работаем с проверенными сетевыми протоколами, которые обеспечивают защиту и конфиденциальность.</p>
				  </div>
				</div>
			</div>
			
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/speed_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Скорость</h5>
					<hr>
					<p class="card-text text-justify">Обеспечение накрутки лайков и фолловеров в кратчайшее время, в зависимости от соц сети и количества.</p>
				  </div>
				</div>
			</div>
			
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/availability_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Доступность</h5>
					<hr>
					<p class="card-text text-justify">Мы предоставляем различные ценовые сегменты, чтобы каждый посетитель мог подобрать тариф для себя.</p>
				  </div>
				</div>
			</div>
			
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/convience_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Удобство</h5>
					<hr>
					<p class="card-text text-justify">Для своих пользователей мы предоставляем широкий список платёжных систем для удобства и привилегий.</p>
				  </div>
				</div>
			</div>
			
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/quality_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Высокое качество</h5>
					<hr>
					<p class="card-text text-justify">Наши услуги и сервисы являются продуктами высокого класса, мы даём гарантию всем своим клиентам.</p>
				  </div>
				</div>
			</div>
			
			<div class = "col-sm-6 col-md-4 col-12 mb-4">
				<div class="card hover-zoom">
				  <img class="card-img-top" src="public/images/resp_logo.png" alt="Card image cap">
				  <div class="card-body">
					<h5 class="card-title text-center">Ответственность за результат</h5>
					<hr>
					<p class="card-text text-justify">Мы являемся ответственными за продажу своей продукции, и нам важно быть лучшими.</p>
				  </div>
				</div>
			</div>
		</div>
	</div>
	
	<div class = "container-fluid orders-part element-animation">
		<h2 class = "text-center mb-3 pt-3 label"> Предоставляемые услуги </h2>
	
		<div class = "row">
			<div class = "col-md-5 col-12">
				<img src = "public/images/phone.png" width = "100%">
			</div>
		
			<div class = "col-md-7 col-12 orders">
				<div class="card border-secondary mt-4">
				  <div class="card-header border-secondary">
					<i class="fa fa-users mr-2 con-picture"></i><h4 class = "con-text"> Накрутка подписчиков </h4>
				  </div>
				  <div class="card-body">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify">Подписчики оказывают основное влияние на рейтинг и качество аккаунта. От количества фолловеров зависит ранжируемость профиля в поиске для других пользователей и вызывает доверие человека, зашедшего на Вашу страничку.</p>
					  <footer class="blockquote-footer">Больше подписчиков - больше доверия! </footer>
					</blockquote>
				  </div>
				</div>
				
				<div class="card border-secondary mt-4 mb-4">
				  <div class="card-header border-secondary">
					<i class="fa fa-heart mr-2 con-picture"></i> <h4 class = "con-text"> Накрутка лайков </h4>
				  </div>
				  <div class="card-body">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify">Накрутка лайков позволит публикациям долгое время находиться топе поиска и гарантирует попадание в рекомендации пользователей Instagram. Большое количество лайков выделяет Вашу публикацию среди других и привлекает много внимания.</p>
					  <footer class="blockquote-footer">Повышается вероятность дополнительных подписок от заинтересованных людей.</footer>
					</blockquote>
				  </div>
				</div>
			</div>
		</div>
	</div>

	<div class = "container-fluid reviews-part element-animation pb-4">
		<div class = "review-items" id = "myReview">
			<h2 class = "text-center mb-3 pt-3 label"> Отзывы клиентов </h2>
			@if (Auth::user())
			<form action="{{ route('makeReview') }}" method="POST" class="mb-2">
			{{ csrf_field() }}
				<label for="name">Текст вашего отзыва</label>
				<textarea required placeholder="Что думаете?" class="form-control" name="review_text" id="review_text"></textarea>
				
				<button class="btn mt-2 form-control rewsubmit" type="submit" style="background-color: #BF7130; color: white">Опубликовать</button>
			</form>
			@else
			<p class="text-center">—Для того чтобы оставить отзыв необходимо авторизоваться—</p>	
			@endif
		</div>
			
		<div id="myCarousel" class="carousel slide carousel-items" data-ride="carousel">	
			<div class="carousel-inner row w-100 mx-auto">
			
			@if (Auth::user() && count(Auth::user()->Reviews) > 0)
				<div class="carousel-item active">
				<div class="card mt-4">
				  <div class="card-header">
					<img src = "https://topsto-crimea.ru/images/detailed/393/1501964039.9383.jpg" width = "10%" class = "round mr-3"><h4 class = "con-text"> {{ Auth::user()->name }} </h4>
				  </div>
				  <div class="card-body review-content">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify"> {{ Auth::user()->Reviews->last()->text }} </p>
					  <?
						function showDate() // $date --> время в формате Unix time
						{
						 $date = strtotime(Auth::user()->Reviews->last()->created_at);
						 $stf = 0;
						 $cur_time = time();
						 $diff = $cur_time - $date;
						  
						 $seconds = array( 'секунда', 'секунды', 'секунд' );
						 $minutes = array( 'минута', 'минуты', 'минут' );
						 $hours = array( 'час', 'часа', 'часов' );
						 $days = array( 'день', 'дня', 'дней' );
						 $weeks = array( 'неделя', 'недели', 'недель' );
						 $months = array( 'месяц', 'месяца', 'месяцев' );
						 $years = array( 'год', 'года', 'лет' );
						 $decades = array( 'десятилетие', 'десятилетия', 'десятилетий' );
						  
						 $phrase = array( $seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades );
						 $length = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );
						  
						 for ( $i = sizeof( $length ) - 1; ( $i >= 0 ) && ( ( $no = $diff / $length[ $i ] ) <= 1 ); $i -- ) {
						 ;
						 }
						 if ( $i < 0 ) {
						 $i = 0;
						 }
						 $_time = $cur_time - ( $diff % $length[ $i ] );
						 $no = floor( $no );
						 $value = sprintf( "%d %s ", $no, getPhrase( $no, $phrase[ $i ] ) );
						  
						 if ( ( $stf == 1 ) && ( $i >= 1 ) && ( ( $cur_time - $_time ) > 0 ) ) {
						 $value .= time_ago( $_time );
						 }
						  
						 return $value . ' назад';
						}
						  
						function getPhrase( $number, $titles ) {
						 $cases = array( 2, 0, 1, 1, 1, 2 );
						  
						 return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
						}	
						
						echo "<footer class='blockquote-footer'>" . showDate() . "</footer>"
					  ?>
					</blockquote>
				  </div>
				</div>
			  </div>
			@else
			  <div class="carousel-item active">
				<div class="card mt-4">
				  <div class="card-header">
					<img src = "https://topsto-crimea.ru/images/detailed/393/1501964039.9383.jpg" width = "10%" class = "round mr-3"><h4 class = "con-text"> Александр Травкин </h4>
				  </div>
				  <div class="card-body review-content">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify">Подписчики оказывают основное влияние на рейтинг и качество аккаунта. От количества фолловеров зависит ранжируемость профиля в поиске для других пользователей и вызывает доверие человека, зашедшего на Вашу страничку.</p>
					  <footer class="blockquote-footer"> 3 минуты назад </footer>
					</blockquote>
				  </div>
				</div>
			  </div>
			@endif
			  <div class="carousel-item">
				<div class="card mt-4">
				  <div class="card-header">
					<img src = "https://topsto-crimea.ru/images/detailed/393/1501964039.9383.jpg" width = "10%" class = "round mr-3"><h4 class = "con-text"> Андрей Горшков </h4>
				  </div>
				  <div class="card-body review-content">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify">Подписчики оказывают основное влияние на рейтинг и качество аккаунта. От количества фолловеров зависит ранжируемость профиля в поиске для других пользователей и вызывает доверие человека, зашедшего на Вашу страничку.</p>
					  <footer class="blockquote-footer"> 5 минут назад </footer>
					</blockquote>
				  </div>
				</div>
			  </div>
			  <div class="carousel-item">
				<div class="card mt-4">
				  <div class="card-header">
					<img src = "https://topsto-crimea.ru/images/detailed/393/1501964039.9383.jpg" width = "10%" class = "round mr-3"><h4 class = "con-text"> Егор Кузнецов </h4>
				  </div>
				  <div class="card-body review-content">
					<blockquote class="blockquote mb-0">
					  <p class = "text-justify">Подписчики оказывают основное влияние на рейтинг и качество аккаунта. От количества фолловеров зависит ранжируемость профиля в поиске для других пользователей и вызывает доверие человека, зашедшего на Вашу страничку.</p>
					  <footer class="blockquote-footer"> 7 минут назад </footer>
					</blockquote>
				  </div>
				</div>
			  </div>
			</div>
			<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
			  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			  <span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
			  <span class="carousel-control-next-icon" aria-hidden="true"></span>
			  <span class="sr-only">Next</span>
			</a>
		  </div>
		</div>
	</div>
	
	<script>
		//window.setTimeout(function() { $(".alert").alert('close'); }, 5000);
	</script>
	
	<style>
		.alert-arrow {
			border: 1px solid #BF7130;
			color: black;
		}
		.alert-arrow .alert-icon {
			position: relative;
			width: 3rem;
			background-color: #BF7130;
		}
		.alert-arrow .alert-icon::after {
			content: "";
			position: absolute;
			width: 0;
			height: 0;
			border-top: .75rem solid transparent;
			border-bottom: .75rem solid transparent;
			border-left: .75rem solid #BF7130;
			right: -.75rem;
			top: 50%;
			transform: translateY(-50%);
		}
		.alert-arrow .close {
			font-size: 1rem;
			color: #BF7130;
		}
		
		@media (max-width:900px){
			#myReview{
				padding-left: 0px;
				padding-right: 0px;
			}
		}
		
		.review-items{
			padding-left: 145px;
			padding-right: 145px;
		}
	</style>
@endsection