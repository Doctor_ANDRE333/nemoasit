@extends('layouts.app')

@section('name')
	Выбор способа платежа
@endsection

@section('content')	
	<div class="container" style = "margin-top: 150px; min-height: 710px;">			<!-- End -->
		@if ($message = Session::get('success'))
			<div class="alert alert-arrow d-flex rounded p-0 fixed-top" style = "margin-top: 82px" role="alert">
				<div class="alert-icon d-flex justify-content-center align-items-center text-white flex-grow-0 flex-shrink-0">
					<i class="fa fa-check"></i>
				</div>
				<div class="alert-message d-flex align-items-center py-2 pl-4 pr-3">
					{!! $message !!}
				</div>
				<a href="#" class="close d-flex ml-auto justify-content-center align-items-center px-3" data-dismiss="alert">
					<i class="fas fa-times"></i>
				</a>
			</div>
			
			<?php Session::forget('success');?>
		@endif

		@if ($message = Session::get('error'))
			<div class="alert alert-arrow alert-arrow-danger d-flex rounded p-0 fixed-top" style = "margin-top: 82px" role="alert">
				<div class="alert-icon d-flex justify-content-center align-items-center text-white flex-grow-0 flex-shrink-0">
					<i class="fas fa-times-circle"></i>
				</div>
				<div class="alert-message d-flex align-items-center py-2 pl-4 pr-3">
					{!! $message !!}
				</div>
				<a href="#" class="close d-flex ml-auto justify-content-center align-items-center px-3" data-dismiss="alert">
					<i class="fas fa-times"></i>
				</a>
			</div>
			
			<?php Session::forget('error');?>
		@endif
		
		<div class="row">
			<div class="col-lg-6 mx-auto">
				<div class="card ">
					<div class="card-header">
						<div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
							<!-- Credit card form tabs -->
							<ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
								<li class="nav-item mr-2"> <a data-toggle="pill" href="#credit-card" class="nav-link active "> <i class="fab fa-paypal mr-2"></i> PayPal </a> </li>
								<li class="nav-item mr-2"> <a data-toggle="pill" href="#paypal" class="nav-link "> <i class="fas fa-money-check mr-2"></i> WebMoney </a> </li>
								<li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link "> <i class="fab fa-yandex mr-2"></i> YooKassa </a> </li>
							</ul>
						</div> <!-- End -->
						<!-- Credit card form content -->
						<div class="tab-content">
							<!-- credit card info-->
							<div id="credit-card" class="tab-pane fade show active pt-3">
								<div class="form-group">
									<h4 class = "label text-center">Варианты оплаты</h4>
									<ul class="list-rectangle">
										<li> <i class="far fa-check-circle mr-1"></i> PayPal</li>
										<li> <i class="far fa-check-circle mr-1"></i> MasterCard</li>
										<li> <i class="far fa-check-circle mr-1"></i> Discover</li>
										<li> <i class="far fa-check-circle mr-1"></i> Visa</li>
										<li> <i class="far fa-check-circle mr-1"></i> Maestro</li>
										<li> <i class="far fa-check-circle mr-1"></i> AmericanExpress</li>
										<li> <i class="far fa-check-circle mr-1"></i> Diners</li>
									</ul>
								</div>
								<div class="card-footer"> 
									<a class="btn continue form-control" style = "color: white" data-toggle="modal" data-target="#paypalModal"> Продолжить </a>
								</div>
						</div> <!-- End -->
						<!-- Paypal info -->
						<div id="paypal" class="tab-pane fade pt-3">
							<h4 class = "label text-center">Варианты оплаты</h4>
							<div class="form-group">
								<ul class="list-rectangle">
									<li> <i class="far fa-check-circle mr-1"></i> WebMoney</li>
									<li> <i class="far fa-check-circle mr-1"></i> Терминалы оплаты</li>
									<li> <i class="far fa-check-circle mr-1"></i> Почта РФ</li>
									<li> <i class="far fa-check-circle mr-1"></i> WebMoney-карта или чек Paymer</li>
									<li> <i class="far fa-check-circle mr-1"></i> Maestro</li>
									<li> <i class="far fa-check-circle mr-1"></i> Visa</li>
									<li> <i class="far fa-check-circle mr-1"></i> Другие карты</li>
								</ul>
							</div>
							<div class="card-footer"> 
								<a class="btn continue form-control" style = "color: white" data-toggle="modal" data-target="#webmoneyModal"> Продолжить </a>
							</div>
						</div> <!-- End -->
						<!-- bank transfer info -->
						<div id="net-banking" class="tab-pane fade pt-3">
							<h4 class = "label text-center">Варианты оплаты</h4>
							<div class="form-group">
								<ul class="list-rectangle">
									<li> <i class="far fa-check-circle mr-1"></i> MasterCard</li>
									<li> <i class="far fa-check-circle mr-1"></i> Maestro</li>
									<li> <i class="far fa-check-circle mr-1"></i> Visa</li>
									<li> <i class="far fa-check-circle mr-1"></i> МИР</li>
									<li> <i class="far fa-check-circle mr-1"></i> Кошелёк ЮMoney</li>
									<li> <i class="far fa-check-circle mr-1"></i> Привязанная карта</li>
								</ul>
							</div>
							<div class="card-footer"> 
								<a class="btn continue form-control" style = "color: white" data-toggle="modal" data-target="#yookassaModal"> Продолжить </a>
							</div>
						</div> <!-- End -->
						<!-- End -->
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
	
	
	<!-- PAYPAL MODAL -->
	<div class="modal fade" id="paypalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title label" id="exampleModalLabel">Оплата с помощью PayPal</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<div class="row">
				<aside class="col" style = "margin: 0 auto">
					<article class="card">
						<div class="card-body p-2">
							@include('common.errors')

							<form role="form" method = "POST" action = "{{route('paypal')}}">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="d-flex mb-2">
										<div><img src="public/images/paypallogo.jpg" width="60" height="60" /></div>
										<div class="mt-1 pl-2"><span class="name"><b>{{ Auth::user()->name }}</b></span>
											<div><span class="cross"><b>****</b></span><span class="pin ml-2"><b>****</b></span></div>
										</div>
									</div>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-ruble-sign"></i></span>
										</div>
										<input type="text" class="form-control" id = "payPalInput" name="amount">
									</div> <!-- input-group.// -->
								</div> <!-- form-group.// -->

								<div class="parent-wrapper">
									<div class="parent">
										<button class="btn child" id = "payPalButton"> Продолжить оплату </button>
									</div>
								</div>
							</form>
						</div> <!-- card-body.// -->
					</article> <!-- card.// -->
				</aside> <!-- col.// -->
			</div> <!-- row.// -->
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Отменить</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- WEBMONEY MODAL -->
	<div class="modal fade" id="webmoneyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title label" id="exampleModalLabel">Оплата с помощью PayPal</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<div class="row">
				<aside class="col" style = "margin: 0 auto">
					<article class="card">
						<div class="card-body p-2">
							@include('common.errors')

							<form role="form" method = "GET" action = "{{route('webmoneyPayments')}}">
								<div class="form-group">
									<div class="d-flex mb-2">
										<div><img src="public/images/wmlogo.png" width="60" height="60" /></div>
										<div class="mt-1 pl-2"><span class="name"><b>{{ Auth::user()->name }}</b></span>
											<div><span class="cross"><b>****</b></span><span class="pin ml-2"><b>****</b></span></div>
										</div>
									</div>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-ruble-sign"></i></span>
										</div>
										<input type="text" class="form-control" id = "webMoneyInput" name="amount">
									</div> <!-- input-group.// -->
								</div> <!-- form-group.// -->

								<div class="parent-wrapper">
									<div class="parent">
										<button class="btn child" id = "webMoneyButton"> Выберите банк  </button>
									</div>
								</div>
							</form>
						</div> <!-- card-body.// -->
					</article> <!-- card.// -->
				</aside> <!-- col.// -->
			</div> <!-- row.// -->
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Отменить</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- YOOKASSA MODAL -->
	<div class="modal fade" id="yookassaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h4 class="modal-title label" id="exampleModalLabel">Оплата с помощью PayPal</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<div class="row">
				<aside class="col" style = "margin: 0 auto">
					<article class="card">
						<div class="card-body p-2">
							@include('common.errors')

							<form role="form" method = "POST" action = "{{route('pay.create')}}">
								{{ csrf_field() }}
								<div class="form-group">
									<div class="d-flex mb-2">
										<div><img src="public/images/yookassalogo.png" width="190" height="60" /></div>
										<div class="mt-1 pl-2"><span class="name"><b>{{ Auth::user()->name }}</b></span>
											<div><span class="cross"><b>****</b></span><span class="pin ml-2"><b>****</b></span></div>
										</div>
									</div>
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="fas fa-ruble-sign"></i></span>
										</div>
										<input type="text" class="form-control" id = "yooKassaInput" name="amount">
									</div> <!-- input-group.// -->
								</div> <!-- form-group.// -->

								<div class="parent-wrapper">
									<div class="parent">
										<button class="btn child" id = "yooKassaButton"> Продолжить оплату </button>
									</div>
								</div>
							</form>
						</div> <!-- card-body.// -->
					</article> <!-- card.// -->
				</aside> <!-- col.// -->
			</div> <!-- row.// -->
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Отменить</button>
		  </div>
		</div>
	  </div>
	</div>
	
	<!-- WEBMONEY BANKS -->
	
	<script>
		window.setTimeout(function() { $(".alert").alert('close'); }, 10000);
		
		function checkMoney(){
			let payPalInput = document.getElementById('payPalInput');
			let webMoneyInput = document.getElementById('webMoneyInput');
			let yooKassaInput = document.getElementById('yooKassaInput');
			
			let payPalButton = document.getElementById('payPalButton');
			let webMoneyButton = document.getElementById('webMoneyButton');
			let yooKassaButton = document.getElementById('yooKassaButton');
			
			//console.log(typeof(webMoneyInput.value));
			
			if (webMoneyInput.value > 0){
				$("#webMoneyButton").prop('disabled', false);
			}
			if (payPalInput.value > 0){
				$("#payPalButton").prop('disabled', false);
			}
			if (yooKassaInput.value > 0){
				$("#yooKassaButton").prop('disabled', false);
			}
			if (!webMoneyInput.value.match(/^\d+$/)){
				$("#webMoneyButton").prop('disabled', true);
			}
			if (!payPalInput.value.match(/^\d+$/)){
				$("#payPalButton").prop('disabled', true);
			}
			if (!yooKassaInput.value.match(/^\d+$/)){
				$("#yooKassaButton").prop('disabled', true);
			}
		}
		
		setInterval(checkMoney, 500);
		//checkMoney();
	</script>   
	
	<style>
		li > i{
			color: green;
		}
	
		.continue{
			background-color: #BF7130;
			color: white;
		}
	
		.rounded {
			border-radius: 1rem
		}

		.nav-pills .nav-link {
			color: black;
			background-color: whitesmoke;
		}

		.nav-pills .nav-link.active {
			color: white;
			background-color: #BF7130;
		}

		.bold {
			font-weight: bold
		}
		
		.list-rectangle {
			list-style: none;
			margin: 0;
			padding: 0;
		}
		
		.list-rectangle>li {
			position: relative;
			display: block;
			margin-bottom: .25rem;
			padding: .325rem .825rem .325rem 1.325rem;
			color: #fff;
			background: black;
		}
		
		.list-rectangle>li:last-child {
			margin-bottom: 0;
		}
		
		.list-rectangle>li::before {
			content: "";
			position: absolute;
			left: 0;
			top: 0;
			bottom: 0;
			width: 0.5rem;
			background: grey;
		}
		
		.balance{
			color: white;
			background-color: #BF7130;
		}
		
		.parent-wrapper {
			height: 100%;
			width: 100%;
		}
		
		.parent {
			display: flex;
			font-size: 0;
			flex-wrap: wrap;
			margin: -10px 0 0 -10px;
		}
		
		.child {
			display: inline-block;
			margin: 10px 0 0 10px;
			flex-grow: 1;
		}
		
		.alert-arrow-danger {
            border: 1px solid #da4932;
            color: #ca452e;
        }

        .alert-arrow-danger .alert-icon {
            background-color: #da4932;
        }

        .alert-arrow-danger .alert-icon::after {
            border-left: .75rem solid #da4932;
        }
		
		.alert-arrow {
			border: 1px solid #BF7130;
			color: black;
		}
		.alert-arrow .alert-icon {
			position: relative;
			width: 3rem;
			background-color: #BF7130;
		}
		.alert-arrow .alert-icon::after {
			content: "";
			position: absolute;
			width: 0;
			height: 0;
			border-top: .75rem solid transparent;
			border-bottom: .75rem solid transparent;
			border-left: .75rem solid #BF7130;
			right: -.75rem;
			top: 50%;
			transform: translateY(-50%);
		}
		.alert-arrow .close {
			font-size: 1rem;
			color: #BF7130;
		}
	</style>
@endsection