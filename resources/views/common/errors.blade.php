<!-- resources/views/common/errors.blade.php -->

@if (count($errors) > 0)
  <!-- ������ ������ ����� -->
  <div class="alert alert-danger">
	<span style = "display: inline-block" onclick="this.parentElement.style.display='none'" class="w3-button w3-green w3-large w3-display-topright">&times;</span>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif