@extends('layouts.app')

@section('name')
	Регистрация
@endsection

@section('content')
	<div class="container main-cont">
		<section class="login-block">
			<div class="container inside">
				<div class="row">
					<div class="col-md-4 login-sec">
						<h2 class="text-center">Register Now</h2>
						<form method="POST" action="{{ route('register') }}" class="login-form">
							{{ csrf_field() }}
							
							<div class="form-group">
								<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder = "Name">
								<i class="fas fa-user user-icon-reg"></i>
								
								@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong> {{ $errors->first('name') }} </strong>
									</span>
								@endif
							</div>
							
							<div class="form-group mt-4">
								<input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder = "Email">
								<i class="fas fa-envelope envelope-icon-reg"></i>
								
								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong> {{ $errors->first('email') }} </strong>
									</span>
								@endif
							</div>
							
							<div class="form-group mt-4">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder = "Password">
								<i class="fas fa-key key-icon-reg"></i>
								
								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong> {{ $errors->first('password') }} </strong>
									</span>
								@endif
							</div>
							
							<div class="form-group mt-4">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder = "Confirm Password">
								<i class="fas fa-check-double check-icon-reg"></i>
							</div>
							
							<div class="form-group">							
								<button type="submit" class="btn btn-login float-left">Register</button>
							</div>
						</form>
					</div>
					<div class="col-md-8 banner-sec">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								<div class="carousel-item active">
									<img class="d-block img-fluid" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">
									<div class="carousel-caption d-none d-md-block">
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg" alt="First slide">
									<div class="carousel-caption d-none d-md-block"></div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</section>
	</div>
@endsection
