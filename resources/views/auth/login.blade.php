@extends('layouts.app')

@section('name')
	Авторизация
@endsection

@section('content')
	<div class="container main-cont">	
		<section class="login-block">
			<div class="container inside">
				<div class="row">
					<div class="col-md-4 login-sec">
						<h2 class="text-center">Login Now</h2>
						<form method="POST" action="{{ route('login') }}" class="login-form">
							{{ csrf_field() }}
							<div class="form-group">
								<input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder = "Email">
								<i class="fas fa-user user-icon-login"></i>
								
								@if ($errors->has('email'))
									<span class="invalid-feedback" role="alert">
										<strong> {{ $errors->first('email') }} </strong>
									</span>
								@endif
							</div>
							
							<div class="form-group mt-4">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder = "Password">
								<i class="fas fa-key key-icon-login"></i>
								
								@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong> {{ $errors->first('password') }} </strong>
									</span>
								@endif
							</div>
							<div class="form-group">
								<div class="main-checkbox">
									<input type="checkbox" value="none" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
									<label for="remember"></label>
								</div>
								
								<span class="text">Remember</span>
								
								<button type="submit" class="btn btn-login float-right">Log in</button>
							</div>
						</form>
					</div>
					<div class="col-md-8 banner-sec">
						<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							</ol>
							<div class="carousel-inner" role="listbox">
								<div class="carousel-item active">
									<img class="d-block img-fluid" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">
									<div class="carousel-caption d-none d-md-block">
									</div>
								</div>
								<div class="carousel-item">
									<img class="d-block img-fluid" src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg" alt="First slide">
									<div class="carousel-caption d-none d-md-block"></div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</section>
	</div>
@endsection
