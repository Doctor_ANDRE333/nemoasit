@extends('layouts.appLK')

@section('title')
	Комментарии к работе
@endsection

@section('content')

<main class="page-content">
    <div class="container">
        @if (Session::has('message'))
            <div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
        @endif

		@include('common.errors')

		@if (count($comments) > 0)
            <div class="card mt-5" id = "cardR">
                <div class="card-header">
                    <h5 class = "text-center"> Просмотр сообщений </h5>
                </div>

                <div class="card-body">
                    <div class = "row mt-1">
                        <div class = "col-12">
                            <div class = "row mt-4">
                                @foreach ($comments as $comment)
                                    <div class = "col-12 mb-2">
                                        @if($comment->user and $comment->user->role != 'comitet')
                                            <div class="alert alert-secondary" role="alert">
                                        @endif
                                            <div class = "row">
                                                <div class = "col-md-1 col-3">
                                                    <div class="chat-img">
                                                        <img alt="Avtar" src="{{URL::asset($comment->user->avatar)}}" width = "100%">
                                                    </div>
                                                </div>

                                                @if(!empty($comment->Answers[0]) && $comment->Answers->last()->user->role != 'comitet')
                                                    <div class = "col-md-6 col-7">
                                                        <div class = "text-left">
                                                            Чат с пользователем: <b> {{ $comment->user->name or 'Пользователь был удалён'}} </b> <br>
                                                        </div>
                                                    </div>

                                                    <?
                                                        $count = 0;

                                                        if (!empty($comment->Answers[0]) && $comment->id == $comment->Answers[0]->comment_id && count($comment->Answers) == 1){
                                                            foreach($comment->Answers as $answer){
                                                                if ($answer->user->type != 'comitet'){
                                                                    $count = 1;
                                                                }
                                                                else{
                                                                    $count = 0;
                                                                    break;
                                                                }
                                                            }
                                                        }

                                                        for($i = count($comment->Answers) - 1; $i > 0; $i--){    //Проверка на комменты
                                                            if ($comment->Answers[$i]->user->role != 'comitet'){
                                                                $count = 1;
                                                            }
                                                            else{
                                                                break;
                                                            }
                                                        }

                                                        if ($count == 1){
                                                            echo "<div class = 'col-md-5 col-2'>";
                                                                echo "<h3 class = 'mt-2 text-right'><span class='badge badge-dark badge-pill'> ! </span></h3>";
                                                            echo "</div>";
                                                        }
                                                    ?>
                                                @else
                                                    <div class = "col-md-10 col-7">
                                                        <div class = "text-left">
                                                            Чат с пользователем: <b> {{ $comment->user->name or 'Пользователь был удалён'}} </b> <br>
                                                        </div>
                                                    </div>

                                                    @if(count($comment->Answers) == 0)
                                                        <div class = "col-md-1 col-2">
                                                            <h3 class = "mt-2"><span class="badge badge-dark badge-pill"> ! </span></h3>
                                                        </div>
                                                    @endif
                                                @endif
                                            </div>

                                            @if (!empty($comment->Answers[0]))
												@for ($i = 0; $i < count($comment->Answers); $i++)
													@if ($i + 1 == count($comment->Answers))
                                                        <hr>

                                                        <div class = "row">
                                                            <div class = "col-md-1 col-3">
                                                                @if ($comment->Answers[$i]->user->role == 'comitet')
                                                                    <div class="chat-img">
                                                                        <img alt="Avtar" src="{{URL::asset($comment->Answers[$i]->user->avatar)}}" width = "100%">
                                                                    </div>
                                                                @else
                                                                    <div class="chat-img">
                                                                        <img alt="Avtar" src="{{URL::asset($comment->Answers[$i]->user->avatar)}}" width = "100%">
                                                                    </div>
                                                                @endif
                                                            </div>

                                                            <div class = "col-md-7 col-9 answer text-left">
                                                                <h5 class = "fiol"> {{ $comment->Answers[$i]->text }} </h5>
                                                            </div>

                                                            <div class = "col-md-4 col-12">
                                                                <div>
                                                                    @if ($comment->Answers[$i]->user->role == 'comitet')
                                                                        <div class = "name-label text-right">
                                                                            Техническая поддержка: <b> {{ $comment->Answers[$i]->user->name }} </b> <br>
                                                                            Время: <b> {{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($comment->Answers[$i]->created_at))) }} </b>
                                                                        </div>
                                                                    @elseif ($comment->Answers[$i]->user->role != 'comitet')
                                                                        <div class = "name-label text-right">
                                                                            Пользователь: <b> {{ $comment->Answers[$i]->user->name }} </b> <br>
                                                                            Время: <b> {{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($comment->Answers[$i]->created_at))) }} </b>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
												    @endif
                                                @endfor
                                            @else
                                                <hr>

                                                <div class = "row">
                                                    <div class = "col-md-1 col-3">
                                                        <div class="chat-img">
                                                            <img alt="Avtar" src="{{URL::asset($comment->user->avatar)}}" width = "100%">
                                                        </div>
                                                    </div>

                                                    <div class = "col-md-7 col-9 answer text-left">
                                                        <h5 class = "fiol"> {{ $comment->text }} </h5>
                                                    </div>

                                                    <div class = "col-md-4 col-12">
                                                        <div>
                                                            <div class = "name-label text-right">
                                                                Пользователь: <b> {{ $comment->user->name }} </b> <br>
                                                                Время: <b> {{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($comment->created_at))) }} </b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>       
										    @endif

                                            <hr>
                                            <div class = "row">
                                                <div class = "col-md-6 col-12">
                                                    <a href = "{{route('indexCurrentChat', $comment->id) }}" class = "btn" style = "width: 200px"> Открыть диалог </a>
                                                </div>
                                            </div>										    
                                        </div>  
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</main>

<style>
    h5, .fiol{
        color:  #BF7130
    }

    .answer, .question{
        min-height: 100px;
        overflow-y: auto;
    }

    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

    .btn{
        background-color:  #BF7130;
        color: white
    }

    b{
        font-size: 16px
    }
</style>

@endsection
