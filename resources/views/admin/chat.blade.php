@extends('layouts.appLK')

@section('title')
	Тех поддержка
@endsection

@section('content')

<main class="page-content">
    <div class="container">
        @if (Session::has('message'))
            <div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
        @endif

		@include('common.errors')
    
        <div class="container content pl-0 pr-0">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        @if (Auth::user()->role != 'comitet')
                            <div class="card-header">Чат с членом техподдержки</div>
                        @else
                            <div class="card-header">Чат с пользователем сайта</div>
                        @endif
                        
                        <div class="card-body height3">
							@if (count($comments) > 0)
                            <ul class="chat-list">
                                @foreach ($comments as $comment)
                                    @if (Auth::user()->role != 'comitet')
                                        @if ($comment->user->role != 'comitet')  
                                            <li class="out">
                                                <div class="chat-img">
                                                    <img alt="Avatar" src="{{URL::asset($comment->user->avatar)}}">
                                                </div>
                                        @else
                                            <li class = "in">
                                                <div class="chat-img">
                                                    <img alt="Avatar" src="{{URL::asset($comment->user->avatar)}}">
                                                </div>
                                        @endif
                                            <div class="chat-body">
                                                <div class="chat-message">
                                                    <h5>{{ $comment->user->name }}</h5>
                                                    <h5><p>{{ $comment->text }}</p></h5>

                                                    <p class = "text-right">{{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($comment->created_at))) }}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @elseif (Auth::user()->role == 'comitet')
                                        @if ($comment->user->role != 'comitet')
                                            <li class="in">
                                                <div class="chat-img">
                                                    <img alt="Avatar" src="{{URL::asset($comment->user->avatar)}}">
                                                </div>
                                        @else
                                            <li class = "out">
                                                <div class="chat-img">
                                                    <img alt="Avatar" src="{{URL::asset($comment->user->avatar)}}">
                                                </div>
                                        @endif
                                            <div class="chat-body">
                                                <div class="chat-message">
                                                    <h5>{{ $comment->user->name }}</h5>
                                                    <h5><p>{{ $comment->text }}</p></h5>

                                                    <p class = "text-right">{{ date('d.m.Y H:i:s', strtotime("+3 hours", strtotime($comment->created_at))) }}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
							@else
								Комментариев пока нет, но вы можете спросить нас!
							@endif
                        </div>
						
                        <div class = "row"> 
                            <div class = "col-12 mt-4">
                                @if (count($comments) > 0)
                                    <form action = "{{ route('SendAnswer', $ThisComment->id) }}" method = "POST">
                                        {{ csrf_field() }}
                                        <div class = "form-group">
                                            <input type = "hidden" value = "{{ Auth::user()->id }}" name = "user_id">
                        
                                            <div class = "row">
                                                <div class="col-10 pl-5 pr-0">
                                                    <textarea type="text" name="text" class="form-control" placeholder="Введите Ваше сообщение" style = "min-height: 70px"></textarea>
                                                </div>
                                                    
                                                <div class="col-2 pl-0">
                                                    <button type="submit" class = "form-control pl-0" style = "border: 0">
                                                        <i class="fab fa-telegram" style = "font-size: 55px; color: #BF7130"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <form action = "{{ route('makeComment') }}" method = "POST">
                                        {{ csrf_field() }}
                                        <div class = "form-group">
                                            <input type = "hidden" value = "{{ Auth::user()->id }}" name = "user_id">
                        
                                            <div class = "row">
                                                <div class="col-10 pl-5 pr-0">
                                                    <textarea type="text" name="text" class="form-control" placeholder="Введите Ваше сообщение" style = "min-height: 70px"></textarea>
                                                </div>
                                                    
                                                <div class="col-2 pl-0">
                                                    <button type="submit" class = "form-control pl-0" style = "border: 0">
                                                        <i class="fab fa-telegram" style = "color: #BF7130;" ></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div> 
                        <div class="card-footer text-center">
                            NemoAS<span style = "color: #BF7130;">IT</span>
                        </div>                       
                    </div>  
                </div>
            </div>
    </div>
</main>

<style>
	button.form-control{
		color: #BF7130;
	}

    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
    }

    .chat-list {
        padding: 0;
        font-size: .8rem;
    }

    .chat-list li {
        margin-bottom: 10px;
        overflow: auto;
        color: #ffffff;
    }

    .chat-list .chat-img {
        float: left;
        width: 48px;
    }

    .chat-list .chat-img img {
        -webkit-border-radius: 50px;
        -moz-border-radius: 50px;
        border-radius: 50px;
        width: 100%;
    }

    .chat-list .chat-message {
        -webkit-border-radius: 50px;
        -moz-border-radius: 50px;
        border-radius: 50px;
        background: #17202b;
        display: inline-block;
        padding: 10px 20px;
        position: relative;
    }

    .chat-list .chat-message:before {
        content: "";
        position: absolute;
        top: 15px;
        width: 0;
        height: 0;
    }

    .chat-list .chat-message h5 {
        margin: 0 0 5px 0;
        font-weight: 600;
        line-height: 100%;
        font-size: .9rem;
    }

    .chat-list .chat-message p {
        line-height: 18px;
        margin: 0;
        padding: 0;
    }

    .chat-list .chat-body {
        margin-left: 20px;
        float: left;
        width: 70%;
    }

    .chat-list .in .chat-message:before {
        left: -12px;
        border-bottom: 20px solid transparent;
        border-right: 20px solid #17202b;
    }

    .chat-list .out .chat-img {
        float: right;
    }

    .chat-list .out .chat-body {
        float: right;
        margin-right: 20px;
        text-align: right;
    }

    .chat-list .out .chat-message {
        background: #BF7130;
    }

    .chat-list .out .chat-message:before {
        right: -12px;
        border-bottom: 20px solid transparent;
        border-left: 20px solid #BF7130;
    }

    .card .card-header:first-child {
        -webkit-border-radius: 0.3rem 0.3rem 0 0;
        -moz-border-radius: 0.3rem 0.3rem 0 0;
        border-radius: 0.3rem 0.3rem 0 0;
    }
    .card .card-header {
        background: #BF7130;
        border: 0;
        font-size: 1rem;
        padding: .65rem 1rem;
        position: relative;
        font-weight: 600;
        color: #ffffff;
    }

    .content{
        margin-top:40px;    
    }
</style>

@endsection
