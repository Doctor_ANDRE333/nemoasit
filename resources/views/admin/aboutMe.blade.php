@extends('layouts.appLK')

@section('title')
	Обо мне
@endsection

@section('content')

<main class="page-content">
    <div class="container">
		@include('common.errors')
		
		@if (Session::has('message'))
			<div class = "alert alert-primary mt-3"> {{ Session::get('message') }} </div>
		@endif
		@if (Session::has('error'))
			<div class = "alert alert-danger mt-3"> {{ Session::get('error') }} </div>
		@endif

		<div class="card text-center" id="cardR">
			<div class="card-header">
				<h5 class="card-title label">Изменение пользовательских данных</h5>
			</div>

			<div class="card-body">
			<form action="{{ route('updateMe', $user->id) }}" method="POST" class="form-horizontal mt-3" enctype="multipart/form-data">
			  {{ csrf_field() }}
				<div class="form-group">
				
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Имя</label></h5>
						  	<input value="{{ $user->name }}" type="text" name="name" id="user-name" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Фамилия</label></h5>
						  	<input value="{{ $user->surname }}" type="text" name="surname" id="user-surname" class="form-control mb-3">
						</div>
					</div>
					
						
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Email</label></h5>
						  	<input value="{{ $user->email }}" type="text" name="email" id="user-email" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Телефон</label></h5>
						  	<input value="{{ $user->phone }}" type="text" name="phone" id="user-phone" class="form-control mb-3">
						</div>
					</div>
					
					<div class="row">
						<div class = "col-sm-6" style = "margin: 0 auto">
							<h5><label for="avatar" class="col control-label mb-0"> Аватар <i class = "far fa-question-circle ml-1" id="icon-tooltip" title="-Размер файла не более 3мб 
-Cоотношение 1:1(квадрат) 
-Форматы *.png *.jpg *.gif"></i></label></h5>
						
							<input type="file" name="avatar" value = "" class="form-control mb-3 ">
						</div>
					</div>



					<div class="row">
						<div class="col-sm-6" style = "margin: 0 auto">
							<h5><label for="user" class="col control-label mb-0">Сменить пароль?</label><h5>
							<select class = "form-control" id = "passCheck" name = "passCheck">
								<option value="0"> Нет </option>
								<option value="1"> Да </option>
							</select>
						</div>
					</div>

					<div class = "passwords">
						<!-- Не стирать, нужно! -->
					</div>

					<div class = "row">
						<div class="col-6" style = "margin: 0 auto">
							<button type="submit" class="btn mt-4 mb-2 form-control">
								<i class="fa fa-plus"></i> Редактировать 
							</button>
						</div>
					</div>
				</div>
			</form>
			</div>

			<div class="card-footer">
				NemoAS<span style = "color: #BF7130;">IT</span>
			</div>
		</div>
    </div>
</main>

<script>
	let Block = document.querySelector('.passwords');
	console.log(Block);

	function Drop(){
		let password_select = document.getElementById('passCheck').value;
		if (password_select != '1'){
			Block.innerHTML = '';
		}
		else if (password_select == '1' && Block.firstChild == null){
			let newDiv = document.createElement('div');
			newDiv.className = 'passwords';
			newDiv.innerHTML = `
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Пароль</label><h5>
					<input value="" type="password" placeholder="Для изменения введите новый или старый пароль" name="password" id="user-password" class="form-control mb-3">
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6" style = "margin: 0 auto">
					<h5><label for="user" class="col control-label mb-0">Подтвердите пароль</label><h5>
					<input value="" type="password" placeholder="Подтвердите пароль" name="password_confirmation" id="user-password-confirmation" class="form-control mb-3">
				</div>
			</div>
			`
			console.log(newDiv);
			Block.append(newDiv);
			}
		}
	
	setInterval(Drop, 250);
	
	$('#icon-tooltip').tooltip();
</script>

<style>	
    body{
        background-image: url(http://pictures.std-1056.ist.mospolytech.ru/blackfon.jpg)
    }

	#cardR{
		background-image: url(http://pictures.std-1056.ist.mospolytech.ru/whitefon.jpg)
	}

	.btn{
		background-color: #BF7130;
		color: white;
	}
</style>
@endsection
