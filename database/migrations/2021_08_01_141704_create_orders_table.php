<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
			$table->integer('order_id');
			$table->integer('user_id')->unsigned();
			$table->integer('price_id')->unsigned();
			$table->integer('amount');
			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('price_id')->references('id')->on('prices');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
